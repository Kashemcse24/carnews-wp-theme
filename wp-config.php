<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'carnews');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&QTvuM~:<W76QcF]fEjtN4Teo-a1!@R@1{YX->!^C[enaVgTDePGoBdd}!g@Bo$2');
define('SECURE_AUTH_KEY',  '(J;ms*K0u>nyy(eBh3-<`r%$-2,@~KKsHiDy_Qtj9gqV<>uq$Js[-5w4uZiAVVgc');
define('LOGGED_IN_KEY',    'tu0`A!yV!>A7`H5n1pc/bhOk__O`~s_m*F0!8$Cm#]fV .z|F+^ZtT6@[mME! !3');
define('NONCE_KEY',        'C.Qx%.r1$I`B*(j/GQ@ oYd3MUhh,:*zkYB 5t$On=vRTbrgWp#Li6+ %5M8r{wO');
define('AUTH_SALT',        '`0[AQug0m|H%5^J&o1u67,ml|`*2<[V?D1PLv]n>8AJ[%eSVb.E4%ivv{*awZ)1?');
define('SECURE_AUTH_SALT', '9?Q-R#i8P8llKiQpPXsQQ3tUwK^L?[g%p0K,6o{nx?q_bPsEJi1P@~.0Q2c.?),R');
define('LOGGED_IN_SALT',   'AD-EcAVjD*HA*~PPj) $G647s 0j[:JPLLdN$%t>h[6^<]J=Y-7+(x)F1OAoO@XV');
define('NONCE_SALT',       'rSq I*`*N2v}hL6vAiQixepaS2QjcnS>Tmezwoq3yU<h.%Ag2_.#D,sl;BP_R:rR');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
