
<?php
//add_theme_support( 'post-thumbnails', array( 'post', 'page') );
//set_post_thumbnail_size( 200, 200, true );

//add_image_size( 'feature-image', 200, 150, true );
//add_image_size( 'your_image_id_here', 480, true );



function myCarnewsMenu() {

	if (function_exists('register_nav_menu')) {
		register_nav_menu( 'header_top_menu', __( 'Header Menu', 'carnews' ) );

		//register_nav_menu( 'footer_menu', __( 'Footer Menu', 'carnews' ) );
		
		//register_nav_menu( 'sidebar_menu', __( 'Sidebar Menu', 'carnews' ) );

	}
}

add_action('init', 'myCarnewsMenu');



function my_widgets_sidebar(){
		//home page sidebar one
		register_sidebar( array(
		'name'          =>esc_html__( 'Home Page Sidebar One', 'carnews' ),
		'description'   =>esc_html__( 'This is sidebar one description .....', 'carnews' ),
		'id'            => 'widget-home-one',
		'before_widget' => '<div class="siderbar-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="sidebar-widget-title">',
		'after_title'   => '</h4> ',
	) );


		register_sidebar( array(
		//footer widget
		'name'          => esc_html__( 'Footer widget', 'carnews' ),
		'description'   =>esc_html__( 'This is footer  description .....', 'carnews' ),
		'id'            => 'footer',
		'before_widget' => '<div class="col-md-3 col-sm-6 col-xs-12"><div class="footer-widget">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2> ',
	) );

		register_sidebar( array(
		//footer bottom contact
		'name'          => esc_html__( 'Footer Contact widget', 'carnews' ),
		'description'   =>esc_html__( 'This is footer contact  description .....', 'carnews' ),
		'id'            => 'footer-contact',
		'before_widget' => '<div class="emergency-call mrb-50">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2> ',
	) );


}

add_action( 'widgets_init', 'my_widgets_sidebar' );



add_theme_support( 'post-thumbnails', array( 'post', 'page', 'Books', 'Products') );
set_post_thumbnail_size( 200, 200, true );
add_image_size( 'feature-image', 300, 300, true );
//add_image_size( 'feature-image', 400, true );


function create_post_type() {
  register_post_type( 'acme_product',
    array(
      'labels' => array(
        'name' => __( 'Products' ),
        'singular_name' => __( 'Product' )
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
}
add_action( 'init', 'create_post_type' );


function people_init() {
	// create a new taxonomy============custom category registration
	register_taxonomy(
		'Product_cat',
		'acme_product',
		array(
			'hierarchical' => true,
			'label' => __( 'Category' ),
			'query_var' => true,
			'rewrite' => array( 'slug' => 'person', 
								'with_front' => false)
			
		)	
	);
}
add_action( 'init', 'people_init' );



add_action( 'init', 'create_client_tax' );
function create_client_tax() {
    register_taxonomy( 
            'client_tag', //your tags taxonomy====custom tag registration
            'acme_product',  // Your post type
            array( 
                'hierarchical'  => false, 
                'label'         => __( 'Tags' ), 
                'singular_name' => __( 'Tag' ), 
                'rewrite'       => true, 
                'query_var'     => true,
				
            )  
      );
}


function codex_custom_init() {
  $labels = array(
    'name' => _x('Books', 'post type general name'),
    'singular_name' => _x('Book', 'post type singular name'),
    'add_new' => _x('Add New', 'book'),
    'add_new_item' => __('Add New Book'),
    'edit_item' => __('Edit Book'),
    'new_item' => __('New Book'),
    'all_items' => __('All Books'),
    'view_item' => __('View Book'),
    'search_items' => __('Search Books'),
    'not_found' =>  __('No books found'),
    'not_found_in_trash' => __('No books found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => __('Books'),
	
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
	'taxonomies' => array('category', 'post_tag'),
    'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
  ); 
  register_post_type('book',$args);
}
add_action( 'init', 'codex_custom_init' );



add_theme_support('post-thumbnails');
add_post_type_support( 'custom_post', 'thumbnail' ); 
add_action( 'init', 'create_post_type1' );
function create_post_type1() {
register_post_type( 'custom_post',
    array(
        'thumbnail',
        'labels' => array(
            'name' => __( 'Custom' ),
            'singular_name' => __( 'Custom' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'custom'),
        'taxonomies' => array('category', 'post_tag'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    )
  );
}


add_theme_support('post-thumbnails');
add_post_type_support( 'my_product', 'thumbnail' );    
function create_post_type2() {
        register_post_type( 'my_product',
            array(
                'labels' => array(
                    'name' => __( ' my 
					
					Products' ),
                    'singular_name' => __( 'Product' )
                ),
                'public' => true,
                'has_archive' => true
            )
        );
    }
    add_action( 'init', 'create_post_type2' );
	
	
	function create_post_type3() {
  register_post_type( 'sadaf_films',
    array(
      'labels' => array(
        'name' => __( 'Films' ),
        'singular_name' => __( 'Film' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array( 'title', 'editor', 'custom-fields','thumbnail' ),
    )
  );
}
add_action( 'init', 'create_post_type3' );



//created widget


/**
 * Adds Foo_Widget widget.
 */
class Foo_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'foo_widget', // Base ID
			esc_html__( 'Widget Title', 'text_domain' ), // Name
			array( 'description' => esc_html__( 'A Foo Widget', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		echo esc_html__( 'Hello, World!', 'text_domain' );
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Foo_Widget

function register_foo_widget() {
    register_widget( 'Foo_Widget' );
}
add_action( 'widgets_init', 'register_foo_widget' );
















